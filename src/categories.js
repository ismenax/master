function openCategory(evt, categoryName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("category-elems");
    for (i of tabcontent) i.style.display = "none";
    tablinks = document.getElementsByClassName("categories-tab_link");
    for(i of tablinks) i.className = i.className.replace(" active", "");
    document.getElementById(categoryName).style.display = "flex";
    evt.currentTarget.className += " active";
  }  
  document.getElementById("defaultOpen").click();